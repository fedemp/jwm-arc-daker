# Arc Darker for JWM

This is a port of the [Arc Dark Theme](https://github.com/horst3180/Arc-theme) for [JWM](http://joewing.net/projects/jwm/).

![](https://gitlab.com/fedemp/jwm-arc-daker/raw/master/screenshot.png)

## How to use

In your `.jwmrc`, include the [`theme`](https://gitlab.com/fedemp/jwm-arc-daker/raw/master/theme) file.

    <JWM>
        <Include>theme</Include>
    </JWM>
    
Beware that the path to the `theme` file should be absolute. By default, includes are resolved ["relative to the location JWM was started"](http://joewing.net/projects/jwm/config.shtml#includes)

## Requirements

* [Arc icon theme](https://github.com/horst3180/arc-icon-theme)
* [Roboto Condensed font](https://fonts.google.com/specimen/Roboto+Condensed) (optional)

## License

The same as JWM. See [LICENSE](https://gitlab.com/fedemp/jwm-arc-daker/raw/master/LICENSE)
